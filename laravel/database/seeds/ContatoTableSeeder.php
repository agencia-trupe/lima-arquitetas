<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'       => 'projetos@limaarquitetas.com.br',
            'endereco'    => '<p>Av. Praiana, 814 &middot; Praia Morro</p><p>29216-090 &middot; Guarapari &middot; ES</p>',
            'telefone'    => '27 3361·2176',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14933.834629854504!2d-40.48284!3d-20.650911!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4a52087bbc4a9da8!2sLima+Arquitetas+Associadas!5e0!3m2!1spt-BR!2sbr!4v1470075578629" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'
        ]);
    }
}
