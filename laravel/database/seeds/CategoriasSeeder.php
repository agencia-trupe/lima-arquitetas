<?php

use Illuminate\Database\Seeder;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projetos_categorias')->insert([
            [
                'ordem'  => 0,
                'titulo' => 'Prédios',
                'slug'   => 'predios',
            ],
            [
                'ordem'  => 1,
                'titulo' => 'Casas',
                'slug'   => 'casas',
            ],
            [
                'ordem'  => 2,
                'titulo' => 'Comerciais',
                'slug'   => 'comerciais',
            ],
            [
                'ordem'  => 3,
                'titulo' => 'Interiores',
                'slug'   => 'interiores',
            ]
        ]);
    }
}
