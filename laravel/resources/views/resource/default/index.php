@extends('<?=$namespace?>.common.template')

@section('content')

    @include('<?=$namespace?>.common.flash')

    <legend>
        <h2>
            <?=$gen->resourceName?>

            <a href="{{ route('<?=$namespace?>.<?=$route?>.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar <?=$gen->unitName?></a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
<?php if($gen->sortable): ?>
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="<?=$gen->table?>">
<?php else: ?>
    <table class="table table-striped table-bordered table-hover table-info">
<?php endif; ?>
        <thead>
            <tr>
<?php if($gen->sortable): ?>
                <th>Ordenar</th>
<?php endif; ?>
<?php foreach($gen->fields as $field): ?>
                <th><?=$field['alias']?></th>
<?php endforeach; ?>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
<?php if($gen->sortable): ?>
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
<?php endif; ?>
<?php foreach($gen->fields as $field): ?>
                <td>{{ $registro-><?=$field['name']?> }}</td>
<?php endforeach; ?>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['<?=$namespace?>.<?=$route?>.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('<?=$namespace?>.<?=$route?>.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

<?php if($gen->paginate): ?>
    {{ $registros->render() }}
<?php endif; ?>

    @endif

@endsection
