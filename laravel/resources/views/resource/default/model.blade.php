namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class {{ $gen->model }} extends Model
{
    protected $table = '{{ $gen->table }}';

    protected $guarded = ['id'];

@if($gen->sortable)
    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
@endif
}
