@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route' => ['painel.projetos.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

        @include('painel.projetos.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
