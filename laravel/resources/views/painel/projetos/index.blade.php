@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Projetos
            <a href="{{ route('painel.projetos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Projeto</a>
        </h2>
    </legend>

    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $categorias, Request::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/projetos']) !!}
        </div>
        @if(!$filtro)
        <div class="col-sm-4">
            <p class="alert alert-info small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione uma categoria para ordenar os projetos.
            </p>
        </div>
        @endif
    </div>

    @if(!count($projetos))
    <div class="alert alert-warning" role="alert">Nenhum projeto cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos">
        <thead>
            <tr>
                @if($filtro)<th>Ordenar</th>@endif
                @if(!$filtro)<th>Categoria</th>@endif
                <th>Título</th>
                <th>Capa</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($projetos as $projeto)
            <tr class="tr-row" id="{{ $projeto->id }}">
                @if($filtro)
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                @endif
                @if(!$filtro)
                <td>
                    @if($projeto->categoria){{ $projeto->categoria->titulo }}@endif
                </td>
                @endif
                <td>
                    {{ $projeto->titulo }}
                </td>
                <td><img src="{{ url('assets/img/projetos/capa/'.$projeto->imagem) }}" alt="" style="width:100%;max-width:200px;height:auto;"></td>
                <td><a href="{{ route('painel.projetos.imagens.index', $projeto->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.projetos.destroy', $projeto->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.projetos.edit', $projeto->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
