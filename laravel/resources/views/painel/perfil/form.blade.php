@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
