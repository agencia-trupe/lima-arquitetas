@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Perfil / Fotos /</small> Adicionar Foto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.perfil.fotos.store', 'files' => true]) !!}

        @include('painel.perfil.fotos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
