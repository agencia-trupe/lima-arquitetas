@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Perfil / Fotos /</small> Editar Foto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.perfil.fotos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.perfil.fotos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
