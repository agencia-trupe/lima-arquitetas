@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Perfil / Imagens /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.perfil.imagens.store', 'files' => true]) !!}

        @include('painel.perfil.imagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
