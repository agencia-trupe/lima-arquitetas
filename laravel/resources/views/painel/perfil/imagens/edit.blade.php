@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Perfil / Imagens /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.perfil.imagens.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.perfil.imagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
