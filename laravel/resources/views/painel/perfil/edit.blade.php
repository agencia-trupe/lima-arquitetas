@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Perfil
            <div class="btn-group pull-right">
            <a href="{{ route('painel.perfil.fotos.index') }}" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-user" style="margin-right:10px;"></span>Editar Fotos</a>
            <a href="{{ route('painel.perfil.imagens.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Editar Imagens</a>
            </div>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.perfil.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.perfil.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
