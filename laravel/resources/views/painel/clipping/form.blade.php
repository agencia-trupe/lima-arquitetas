@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control monthpicker']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem de Capa') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/clipping/thumbs/'.$clipping->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clipping.index') }}" class="btn btn-default btn-voltar">Voltar</a>
