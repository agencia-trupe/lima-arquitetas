@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.clipping.index') }}" title="Voltar para Clipping" class="btn btn-sm btn-default">
        &larr; Voltar para Clipping
    </a>

    <legend>
        <h2><small>Clipping /</small> Imagem Fixa</h2>
    </legend>

    {!! Form::model($imagem, [
        'route'  => ['painel.clipping.imagem.update', $imagem->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.common.flash')

    <div class="well form-group">
        {!! Form::label('imagem', 'Imagem') !!}
        @if($imagem->imagem)
        <img src="{{ url('assets/img/clipping/'.$imagem->imagem) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px;">
        @endif
        {!! Form::file('imagem', ['class' => 'form-control']) !!}
    </div>

    {!! Form::submit('Alterar', ['class' => 'btn btn-success']) !!}

    {!! Form::close() !!}

@endsection
