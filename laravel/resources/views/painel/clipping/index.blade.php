@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Clipping
            <a href="{{ route('painel.clipping.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Clipping</a>
        </h2>
    </legend>

    @if(!count($clipping))
    <div class="alert alert-warning" role="alert">Nenhum clipping cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Título</th>
                <th>Capa</th>
                <th>Imagens</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($clipping as $clipping)
            <tr class="tr-row" id="{{ $clipping->id }}">
                <td>{{ $clipping->data }}</td>
                <td>{{ $clipping->titulo }}</td>
                <td><img src="{{ url('assets/img/clipping/thumbs/'.$clipping->imagem) }}" alt="" style="width:100%;max-width:100px;height:auto;"></td>
                <td><a href="{{ route('painel.clipping.imagens.index', $clipping->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.clipping.destroy', $clipping->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.clipping.edit', $clipping->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
