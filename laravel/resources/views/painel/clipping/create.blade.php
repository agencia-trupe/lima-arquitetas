@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Clipping /</small> Adicionar Clipping</h2>
    </legend>

    {!! Form::open(['route' => 'painel.clipping.store', 'files' => true]) !!}

        @include('painel.clipping.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
