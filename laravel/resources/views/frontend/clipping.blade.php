@extends('frontend.common.template')

@section('content')

    <div class="clipping">
        @foreach($clipping as $thumb)
        <a href="#" class="thumb" data-galeria="{{ $thumb->id }}">
            <img src="{{ asset('assets/img/clipping/thumbs/'.$thumb->imagem) }}" alt="">
            <div class="overlay">
                <span>
                    {{ $thumb->titulo }}
                </span>
            </div>
        </a>
        @endforeach
    </div>

    <div class="hidden">
        @foreach($clipping as $registro)
            @foreach($registro->imagens as $imagem)
                <a href="{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $registro->id }}"></a>
            @endforeach
        @endforeach
    </div>

@endsection
