@extends('frontend.common.template')

@section('content')

    <div class="projetos">
        <div class="projetos-categorias">
            @foreach($categorias as $cat)
            <a href="{{ route('projetos', $cat->slug) }}" @if($cat->id === $categoria->id) class="active" @endif>{{ $cat->titulo }}</a>
            @endforeach
        </div>

        @foreach($projetos as $thumb)
        <a href="#" class="thumb" data-galeria="{{ $thumb->id }}">
            <img src="{{ asset('assets/img/projetos/capa/'.$thumb->imagem) }}" alt="">
            <div class="overlay">
                <span>{{ $thumb->titulo }}</span>
            </div>
        </a>
        @endforeach
    </div>

    <div class="hidden">
        @foreach($projetos as $registro)
            @foreach($registro->imagens as $imagem)
                <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $registro->id }}" title="{{ strtoupper($registro->titulo) }} {{ $registro->ano ? '- '.$registro->ano : '' }} {{ $registro->area ? '- '.$registro->area : '' }}"></a>
            @endforeach
        @endforeach
    </div>

@endsection
