    <header @if(Route::currentRouteName() === 'home') class="home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>

            <nav id="nav-desktop">
                @include('frontend.common._nav')
            </nav>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>

            @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
            @endif
        </div>

        <nav id="nav-mobile">
            @include('frontend.common._nav')
        </nav>
    </header>
