<a href="{{ route('home') }}" @if(Route::currentRouteName() === 'home') class="active" @endif>
    HOME
</a>
<a href="{{ route('perfil') }}" @if(Route::currentRouteName() === 'perfil') class="active" @endif>
    PERFIL
</a>
<a href="{{ route('projetos') }}" @if(Route::currentRouteName() === 'projetos') class="active" @endif">
    PROJETOS
</a>
<a href="{{ route('clipping') }}" @if(Route::currentRouteName() === 'clipping') class="active" @endif">
    CLIPPING
</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif">
    CONTATO
</a>
