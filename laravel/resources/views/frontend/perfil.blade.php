@extends('frontend.common.template')

@section('content')

    <div class="perfil">
        <div class="esquerda">
            <div class="fotos-slider">
                @foreach($fotos as $foto)
                    <img src="{{ asset('assets/img/perfil/'.$foto->imagem) }}" alt="">
                @endforeach

                @if(count($fotos) > 1)
                <a href="#" class="controls cycle-prev"></a>
                <a href="#" class="controls cycle-next"></a>
                @endif
            </div>
        </div>

        <div class="direita">
            <div class="imagens">
                @foreach($imagens as $imagem)
                    <img src="{{ asset('assets/img/perfil/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>

            <div class="texto">
                {!! $perfil->texto !!}
            </div>
        </div>
    </div>

@endsection
