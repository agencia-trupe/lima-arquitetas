@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="informacoes">
            <p class="telefone">{{ $contato->telefone }}</p>
            <div class="endereco">
                {!! $contato->endereco !!}
            </div>
            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="submit" value="ENVIAR">
                <div class="response">
                    <div id="form-contato-response"></div>
                </div>
            </form>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
