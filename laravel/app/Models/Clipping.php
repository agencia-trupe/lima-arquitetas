<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Carbon\Carbon;
use App\Helpers\CropImage;

class Clipping extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'clipping';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ClippingImagem', 'clipping_id')->ordenados();
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('m/Y', $date)->format('Y-m');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m', $date)->format('m/Y');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'   => 475,
            'height'  => 300,
            'path'    => 'assets/img/clipping/thumbs/',
        ]);
    }
}
