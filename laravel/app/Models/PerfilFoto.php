<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class PerfilFoto extends Model
{
    protected $table = 'perfil_fotos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            'width'  => 980,
            'height' => null,
            'path'   => 'assets/img/perfil/'
        ]);
    }
}
