<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('projetos/{projetos_categoria?}', 'ProjetosController@index')->name('projetos');
    Route::get('clipping', 'ClippingController@index')->name('clipping');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@envio')->name('contato.envio');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('banners', 'BannersController');
        Route::resource('perfil/imagens', 'PerfilImagensController');
		Route::resource('perfil/fotos', 'PerfilFotosController');
        Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
        // Route::resource('projetos/categorias', 'ProjetosCategoriasController');
        Route::resource('projetos', 'ProjetosController');
        Route::get('projetos/{projetos}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::resource('projetos.imagens', 'ProjetosImagensController');
        Route::resource('clipping', 'ClippingController');
        Route::get('clipping/{clipping}/imagens/clear', [
            'as'   => 'painel.clipping.imagens.clear',
            'uses' => 'ClippingImagensController@clear'
        ]);
        Route::resource('clipping.imagens', 'ClippingImagensController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
