<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerfilImagensRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
