<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjetoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'projetos_categoria_id' => 'required',
            'imagem'                => 'required|image',
            'ano'                   => '',
            'area'                  => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
