<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index(ProjetoCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = ProjetoCategoria::ordenados()->first() ?: \App::abort('404');
        }

        $categorias = ProjetoCategoria::ordenados()->get();
        $projetos   = $categoria->projetos()->get();

        return view('frontend.projetos', compact('categorias', 'categoria', 'projetos'));
    }
}
