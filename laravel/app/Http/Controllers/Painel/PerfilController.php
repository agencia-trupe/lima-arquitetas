<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerfilRequest;
use App\Http\Controllers\Controller;

use App\Models\Perfil;

class PerfilController extends Controller
{
    public function index()
    {
        $registro = Perfil::first();

        return view('painel.perfil.edit', compact('registro'));
    }

    public function update(PerfilRequest $request, Perfil $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            // if (isset($input['imagem'])) $input['imagem'] = Perfil::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.perfil.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
