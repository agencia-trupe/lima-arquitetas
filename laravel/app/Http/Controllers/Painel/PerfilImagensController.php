<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerfilImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\PerfilImagem;

class PerfilImagensController extends Controller
{
    public function index()
    {
        $registros = PerfilImagem::ordenados()->get();

        return view('painel.perfil.imagens.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.perfil.imagens.create');
    }

    public function store(PerfilImagensRequest $request)
    {
        try {

            if (PerfilImagem::count() >= 4) {
                throw new \Exception('Limite de imagens excedido', 1);
            }

            $input = $request->all();
            $input['imagem'] = PerfilImagem::uploadImagem();

            PerfilImagem::create($input);
            return redirect()->route('painel.perfil.imagens.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(PerfilImagem $registro)
    {
        return view('painel.perfil.imagens.edit', compact('registro'));
    }

    public function update(PerfilImagensRequest $request, PerfilImagem $registro)
    {
        try {

            $input = $request->all();
            $input['imagem'] = PerfilImagem::uploadImagem();

            $registro->update($input);
            return redirect()->route('painel.perfil.imagens.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(PerfilImagem $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.perfil.imagens.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
