<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BannersRequest;
use App\Http\Controllers\Controller;

use App\Models\Banner;

class BannersController extends Controller
{
    public function index()
    {
        $registros = Banner::ordenados()->get();

        return view('painel.banners.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.banners.create');
    }

    public function store(BannersRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = Banner::uploadImagem();

            Banner::create($input);
            return redirect()->route('painel.banners.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Banner $registro)
    {
        return view('painel.banners.edit', compact('registro'));
    }

    public function update(BannersRequest $request, Banner $registro)
    {
        try {

            $input = $request->all();
            $input['imagem'] = Banner::uploadImagem();

            $registro->update($input);
            return redirect()->route('painel.banners.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Banner $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.banners.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
