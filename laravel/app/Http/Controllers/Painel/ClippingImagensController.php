<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingImagemRequest;
use App\Http\Controllers\Controller;

use App\Models\Clipping;
use App\Models\ClippingImagem;

class ClippingImagensController extends Controller
{
    public function index(Clipping $clipping)
    {
        $imagens = ClippingImagem::clipping($clipping->id)->ordenados()->get();

        return view('painel.clipping.imagens.index', compact('imagens', 'clipping'));
    }

    public function show(Clipping $clipping, ClippingImagem $imagem)
    {
        return $imagem;
    }

    public function create(Clipping $clipping)
    {
        return view('painel.clipping.imagens.create', compact('clipping'));
    }

    public function store(Clipping $clipping, ClippingImagemRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ClippingImagem::uploadImagem();
            $input['clipping_id'] = $clipping->id;

            $imagem = ClippingImagem::create($input);

            $view = view('painel.clipping.imagens.imagem', compact('clipping', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Clipping $clipping, ClippingImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.clipping.imagens.index', $clipping)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Clipping $clipping)
    {
        try {

            $clipping->imagens()->delete();
            return redirect()->route('painel.clipping.imagens.index', $clipping)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
