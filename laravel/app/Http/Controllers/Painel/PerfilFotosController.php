<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerfilFotosRequest;
use App\Http\Controllers\Controller;

use App\Models\PerfilFoto;

class PerfilFotosController extends Controller
{
    public function index()
    {
        $registros = PerfilFoto::ordenados()->get();

        return view('painel.perfil.fotos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.perfil.fotos.create');
    }

    public function store(PerfilFotosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = PerfilFoto::uploadImagem();

            PerfilFoto::create($input);
            return redirect()->route('painel.perfil.fotos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(PerfilFoto $registro)
    {
        return view('painel.perfil.fotos.edit', compact('registro'));
    }

    public function update(PerfilFotosRequest $request, PerfilFoto $registro)
    {
        try {

            $input = $request->all();
            $input['imagem'] = PerfilFoto::uploadImagem();

            $registro->update($input);
            return redirect()->route('painel.perfil.fotos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(PerfilFoto $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.perfil.fotos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
