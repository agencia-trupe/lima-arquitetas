<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Perfil;
use App\Models\PerfilImagem;
use App\Models\PerfilFoto;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil  = Perfil::first();
        $imagens = PerfilImagem::ordenados()->get();
        $fotos   = PerfilFoto::ordenados()->get();

        return view('frontend.perfil', compact('perfil', 'imagens', 'fotos'));
    }
}
